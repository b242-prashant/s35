const express = require("express");
//import mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
/*
	Syntax:
		mongoose.connect("<MongoDB atlas connection string>", 
		{ 
			useNewUrlParser : true 
		})
*/
mongoose.connect("mongodb+srv://Prashant_R:TeaLXbXdHCFwzfo6@zuitt-bootcamp.yuxjdq2.mongodb.net/b242_to-do?retryWrites=true&w=majority",
	{	
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

//if a connection error occured, output will be in the console
db.on("error", console.error.bind(console,"connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);


app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/tasks",(req,res) =>{
	Task.findOne({name : req.body.name}, (err,result) => {
		if(result !=null && result.name === req.body.name){
			return res.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})

app.get("/tasks",(req,res) =>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));

// List out all the registered users