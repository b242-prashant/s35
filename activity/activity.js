const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;

app.use(express.json());

app.use(express.urlencoded({extended:true}));


mongoose.connect("mongodb+srv://Prashant_R:TeaLXbXdHCFwzfo6@zuitt-bootcamp.yuxjdq2.mongodb.net/b242_to-do?retryWrites=true&w=majority",
	{	
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console,"connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

const User = mongoose.model("User", userSchema);

app.post("/signup",(req,res) =>{
	User.findOne({username : req.body.username}, (err,result) => {
		if(result !=null && result.username === req.body.username){
			return res.send("User already exists");
		}
		else{
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})

app.get("/Users",(req,res) =>{
	User.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));